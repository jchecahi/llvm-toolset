#
# Build/PR gating tests for *LLVM 13*
#
# Imports and runs tests provided by Fedora LLVM git for the matching LLVM version.
#
# NOTE: *always* keep this file in sync with upstream, i.e. Fedora. Since we cannot "discover" a plan,
# we must duplicate at least some part of upstream plan setup, like `adjust` or `provision`. Not necessarily
# all steps, but if we do need some of them here, let's focus on making changes in upstream first, to preserve
# one source of truth. Once TMT learns to include whole plans, we could drop the copied content from here.
#

summary: libomp clang tests for build/PR gating
adjust:
  - because: "Plan to be ran when either executed locally, or executed by CI system to gate a build or PR."
    when: >-
      trigger is defined
      and trigger != commit
      and trigger != build
    enabled: false

  - because: "When testing SCL-ized LLVM, the collection must be enabled first"
    environment+:
      WITH_SCL: "scl enable llvm-toolset-13.0"
    when: "collection == llvm-toolset-13.0"

  - because: "libomp not supported in s390x"
    when: arch == s390x
    enabled: false

discover:
    - name: clang-upstream
      how: fmf
      url: https://src.fedoraproject.org/rpms/clang.git
      ref: rawhide
      test: libomp
execute:
    how: tmt
prepare:
    # We want to make sure libomp is not already present on the system to ensure
    # that clang pulls in the correct libomp dependencies when it is installed.
    - name: Drop libomp
      how: shell
      script: |
        yum erase -y libomp libomp-devel clang clang-libs
provision:
  hardware:
    memory: ">= 4 GiB"